package com.keywcorp.ccis.irad.nifi.processors.util;

import org.apache.commons.io.IOUtils;
import org.apache.nifi.logging.ProcessorLog;

import java.io.File;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.InputStream;
import java.util.concurrent.ConcurrentHashMap;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;

/**
 * Created by matthewloppatto on 2/23/16.
 */
public class ParserClassLoader extends ClassLoader {

    // maintain a cache of the classes loaded from the local repo
    private ConcurrentHashMap<String, LoadedClass> classes = new ConcurrentHashMap<>();

    // this should be NIFI_HOME
    private String localRepoPath;

    private ProcessorLog log;

    public ParserClassLoader(ClassLoader parent, String nifiHome, ProcessorLog log) {
        super(parent);
        localRepoPath = nifiHome + File.separator + "lib" + File.separator + "ext";
        this.log = log;
        log.info("ParserClassLoader instantiated with localRepoPath={}", new Object[]{localRepoPath});
    }

    public void setLocalRepoPath(String path) {
        this.localRepoPath = path + File.separator + "lib" + File.separator + "ext";
        log.info("localRepoPath is now {}", new Object[]{localRepoPath});
    }

    @Override
    public synchronized Class<?> loadClass(String name) throws ClassNotFoundException {
        Class c = null;

        log.info("Checking with parent classloader");

        // check with the parent class loader first
        try {
            log.info("getParent() is {}", new Object[]{getParent()});
            if(getParent() != null) c = getParent().loadClass(name);
            log.info("Parent class loader returned class: {}", new Object[]{c});
            return c;
        } catch(ClassNotFoundException e) {
            // do nothing
            log.info("Did not find class {} in parent class loader", new Object[]{name});
        }

        log.info("Checking local cache");

        // check the local cache
        // skip local cache if the a more recent version of the jar file exists
        LoadedClass lc = classes.get(name);
        if(lc != null) {
            c = lc.getClazz();
            File originalJarFile = new File(lc.getJarPath());
            log.info("originalJarFile parent is {}", new Object[]{originalJarFile.getParent()});
            if(originalJarFile.exists() &&
               originalJarFile.getParent() != null &&
               originalJarFile.getParent().equals(localRepoPath) &&
               (lc.getLastModified() >= originalJarFile.lastModified()) &&
               c != null) {
                log.info("Found class {} in local cache", new Object[]{name});
                return c;
            }
        }

        log.info("Checking local repo");

        // check the local repository
        File localRepo = new File(localRepoPath);
        if(!localRepo.isDirectory()) {
            log.error("Local repository {} is not a directory!", new Object[]{localRepoPath});
            throw new ClassNotFoundException();
        }
        File[] jars = localRepo.listFiles(new FilenameFilter() {
            @Override
            public boolean accept(File dir, String name) {
                return name.endsWith(".jar");
            }
        });
        log.info("Found {} jar files to search in {}", new Object[]{jars.length, localRepoPath});
        byte[] classData;
        String className = name.replace(".", "/") + ".class";
        for(File jar : jars) {
            try {
                log.info("Searching {} for class {}.class", new Object[]{jar.getName(), name});
                JarFile jarFile = new JarFile(jar);
                JarEntry entry = jarFile.getJarEntry(className);
                if (entry == null) continue;
                log.info("Found class {}!", new Object[]{name});
                InputStream is = jarFile.getInputStream(entry);
                classData = IOUtils.toByteArray(is);
            } catch(IOException e) {
                continue;
            }
            c = defineClass(name, classData, 0, classData.length);
            classes.put(name, new LoadedClass(c, jar.getAbsolutePath(), jar.lastModified()));
            return c;
        }

        log.info("Class not found by ParserClassLoader!");

        throw new ClassNotFoundException();
    }

    /**
     * A Class that has been loaded into the local cache from the local repository.  Additional information about the
     * Class is stored such as the path of its jar file and the last modified time of the jar when the class was loaded.
     * When loading a class from the local cache, the last modified time in LoadedClass is checked against the current
     * jar file's last modified time.  If there is a newer jar file that exists, the class is reloaded from the local
     * repository.
     */
    private static class LoadedClass {

        private Class c;
        private String jarPath;
        private long lastModified; // at the time the class was loaded

        public LoadedClass(Class c, String jarPath, long lastModified) {
            this.c = c;
            this.jarPath = jarPath;
            this.lastModified = lastModified;
        }

        public Class getClazz() {
            return c;
        }

        public String getJarPath() {
            return jarPath;
        }

        public long getLastModified() {
            return lastModified;
        }

    }

}
