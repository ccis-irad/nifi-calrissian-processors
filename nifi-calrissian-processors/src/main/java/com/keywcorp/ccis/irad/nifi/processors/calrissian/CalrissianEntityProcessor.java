package com.keywcorp.ccis.irad.nifi.processors.calrissian;

import com.keywcorp.ccis.irad.nifi.processors.util.Parser;
import org.apache.accumulo.core.client.*;
import org.apache.nifi.annotation.documentation.CapabilityDescription;
import org.apache.nifi.annotation.documentation.Tags;
import org.apache.nifi.flowfile.FlowFile;
import org.apache.nifi.processor.ProcessContext;
import org.apache.nifi.processor.ProcessSession;
import org.apache.nifi.processor.exception.ProcessException;
import org.apache.nifi.processor.io.InputStreamCallback;
import org.apache.nifi.stream.io.StreamUtils;
import org.calrissian.accumulorecipes.graphstore.GraphStore;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

/**
 * Created by matthewloppatto on 2/25/16.
 */
@Tags({"Calrissian", "Accumulo", "Entity", "Graph"})
@CapabilityDescription("Write entities to the Calrissian entity/graph store.")
public class CalrissianEntityProcessor extends AbstractCalrissianProcessor {

    public CalrissianEntityProcessor() {}

    @Override
    public void onTrigger(ProcessContext context, ProcessSession session) throws ProcessException {
        final List<FlowFile> flowFiles = session.get(context.getProperty(BATCH_SIZE).asInteger());
        if (flowFiles.isEmpty()) {
            return;
        }

        GraphStore store = null;
        try {
            store = getGraphStore(context);
        } catch (TableExistsException | AccumuloSecurityException | AccumuloException | TableNotFoundException e) {
            getLogger().error("Error getting graph store.", e);
            session.transfer(flowFiles, REL_FAILURE);
            return;
        }

        for(FlowFile flowFile : flowFiles) {
            // Read the contents of the FlowFile into a byte array
            final byte[] bytes = new byte[(int) flowFile.getSize()];
            session.read(flowFile, new InputStreamCallback() {
                @Override
                public void process(final InputStream in) throws IOException {
                    StreamUtils.fillBuffer(in, bytes, true);
                }
            });
            Parser p;
            try {
                getLogger().info("Attempting to search for class {}", new Object[]{context.getProperty(PARSER_CLASS).getValue()});
                Class c = PARSER_CLASS_LOADER.loadClass(context.getProperty(PARSER_CLASS).getValue());
                getLogger().info("ParserClassLoader returned {}", new Object[]{c});
                p = (Parser)c.newInstance();
                getLogger().info("Casted class to Parser");
            } catch(Exception e) {
                getLogger().error("Failed to load parser", e);
                throw new ProcessException(e);
            }
            try {
                getLogger().info("Parsing flowfile data");
                p.parse(bytes);
                store.save(p.getEntities());
                session.transfer(flowFile, REL_SUCCESS);
                session.getProvenanceReporter().send(flowFile, "Calrissian Entity/Graph Store");
            } catch(Exception e) {
                getLogger().error("Failed to parse event.", e);
                session.transfer(flowFile, REL_FAILURE);
            }
        }

        try {
            // TODO: There is still a chance the flush could fail and some events would not get written.  The flowfiles
            // TODO: that contain those events should be sent to REL_FAILURE.
            store.flush();
            store.shutdown();
        } catch (Exception e) {
            getLogger().error("Error flushing or shutting down graph store", e);
        }
    }

}
