package com.keywcorp.ccis.irad.nifi.processors.util;

import org.calrissian.mango.domain.entity.Entity;
import org.calrissian.mango.domain.event.Event;

import java.util.List;

/**
 * Created by matthewloppatto on 2/24/16.
 */
public interface Parser {

    /**
     * Parses the byte array data in the Nifi flowfile.
     *
     * @param bytes The Nifi flowfile data
     * @throws Exception When a parsing error occurs.  This will be handled by the Nifi processor.
     */
    void parse(byte[] bytes) throws Exception;

    /**
     * Lazily instantiate the events.
     *
     * @return The events represented in this netflow record.
     */
    List<Event> getEvents();

    /**
     * Lazily instantiate the graph.
     *
     * @return The graph of vertices and edges represented in this netflow record.
     */
    List<Entity> getEntities();

}
