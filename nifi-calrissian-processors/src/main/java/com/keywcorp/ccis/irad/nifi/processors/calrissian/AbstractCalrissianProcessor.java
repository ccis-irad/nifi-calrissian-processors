package com.keywcorp.ccis.irad.nifi.processors.calrissian;

import com.keywcorp.ccis.irad.nifi.processors.util.ParserClassLoader;
import org.apache.accumulo.core.client.*;
import org.apache.accumulo.core.client.security.tokens.PasswordToken;
import org.apache.nifi.annotation.lifecycle.OnAdded;
import org.apache.nifi.components.PropertyDescriptor;
import org.apache.nifi.processor.AbstractProcessor;
import org.apache.nifi.processor.ProcessContext;
import org.apache.nifi.processor.Relationship;
import org.apache.nifi.processor.util.StandardValidators;
import org.calrissian.accumulorecipes.eventstore.impl.AccumuloEventStore;
import org.calrissian.accumulorecipes.graphstore.GraphStore;
import org.calrissian.accumulorecipes.graphstore.impl.AccumuloEntityGraphStore;

import java.io.File;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Created by matthewloppatto on 2/25/16.
 */
public abstract class AbstractCalrissianProcessor extends AbstractProcessor {

    static final PropertyDescriptor NIFI_HOME = new PropertyDescriptor.Builder()
            .name("Nifi Home")
            .description("The Nifi home directory.")
            .required(true)
            .addValidator(StandardValidators.NON_EMPTY_VALIDATOR)
            .addValidator(StandardValidators.FILE_EXISTS_VALIDATOR)
            .build();

    static final PropertyDescriptor PARSER_CLASS = new PropertyDescriptor.Builder()
            .name("Parser Class")
            .description("The fully qualified parser class name to use for reading and parsing the flow file data." +
                    "This parser class should implement com.keywcorp.ccis.irad.nifi.processors.util.Parser and should exist in NIFI_HOME/lib/ext." +
                    "Example: com.mycompany.MyClass")
            .required(true)
            .addValidator(StandardValidators.NON_EMPTY_VALIDATOR)
            .build();

    static final PropertyDescriptor INSTANCE_NAME = new PropertyDescriptor.Builder()
            .name("Accumulo Instance Name")
            .description("The name of the Accumulo Instance to connect to")
            .required(true)
            .addValidator(StandardValidators.NON_EMPTY_VALIDATOR)
            .build();

    static final PropertyDescriptor ZOOKEEPER_CONNECT_STRING = new PropertyDescriptor.Builder()
            .name("ZooKeeper Connection String")
            .description("A comma-separated list of ZooKeeper hostname:port pairs")
            .required(true)
            .defaultValue("localhost:2181")
            .addValidator(StandardValidators.NON_EMPTY_VALIDATOR)
            .build();

    static final PropertyDescriptor USERNAME = new PropertyDescriptor.Builder()
            .name("Username")
            .description("The username to use when connecting to Accumulo")
            .required(true)
            .addValidator(StandardValidators.NON_EMPTY_VALIDATOR)
            .build();
    static final PropertyDescriptor PASSWORD = new PropertyDescriptor.Builder()
            .name("Password")
            .description("The password to use when connecting to Accumulo")
            .required(true)
            .sensitive(true)
            .addValidator(StandardValidators.NON_EMPTY_VALIDATOR)
            .build();
    static final PropertyDescriptor BATCH_SIZE = new PropertyDescriptor.Builder()
            .name("Batch Size")
            .description("Number of FlowFiles to send in a single batch. Accumulo does not provide information about which data fails in the case of a batch operation. "
                    + "Therefore, if any FlowFile fails in the batch, all may be routed to failure, even if they were already sent successfully to Accumulo. "
                    + "If this is problematic for your use case, use a Batch Size of 1.")
            .required(true)
            .expressionLanguageSupported(false)
            .defaultValue("1")
            .addValidator(StandardValidators.POSITIVE_INTEGER_VALIDATOR)
            .build();

    static final Relationship REL_SUCCESS = new Relationship.Builder()
            .name("success")
            .description("A FlowFile is routed to this relationship after it has been sent to Accumulo")
            .build();
    static final Relationship REL_FAILURE = new Relationship.Builder()
            .name("failure")
            .description("A FlowFile is routed to this relationship if it cannot be sent to Accumulo")
            .build();

    static ParserClassLoader PARSER_CLASS_LOADER;

    public AbstractCalrissianProcessor() {}

    @OnAdded
    public void onAdded() {
        getLogger().info("Calling onAdded()");
        if(PARSER_CLASS_LOADER == null) {
            PARSER_CLASS_LOADER = new ParserClassLoader(this.getClass().getClassLoader(), "", getLogger());
        }
    }

    @Override
    public void onPropertyModified(PropertyDescriptor descriptor, String oldValue, String newValue) {
        if(descriptor.equals(NIFI_HOME) && newValue != null) {
            getLogger().info("Value for NIFI_HOME changed from {} to {}", new Object[]{oldValue, newValue});
            File f = new File(newValue);
            if(f.exists() && f.isDirectory()) {
                getLogger().info("Updating ParserClassLoader repo path to {}", new Object[]{newValue});
                if(PARSER_CLASS_LOADER == null) {
                    PARSER_CLASS_LOADER = new ParserClassLoader(this.getClass().getClassLoader(), newValue, getLogger());
                }
                PARSER_CLASS_LOADER.setLocalRepoPath(newValue);
            } else {
                getLogger().error("Value for NIFI_HOME does not exist or is not a directory: {}", new Object[]{newValue});
            }
        }
    }

    @Override
    protected List<PropertyDescriptor> getSupportedPropertyDescriptors() {
        final List<PropertyDescriptor> properties = new ArrayList<>();
        properties.add(NIFI_HOME);
        properties.add(PARSER_CLASS);
        properties.add(INSTANCE_NAME);
        properties.add(ZOOKEEPER_CONNECT_STRING);
        properties.add(USERNAME);
        properties.add(PASSWORD);
        properties.add(BATCH_SIZE);
        return properties;
    }

    @Override
    public Set<Relationship> getRelationships() {
        final Set<Relationship> relationships = new HashSet<>();
        relationships.add(REL_SUCCESS);
        relationships.add(REL_FAILURE);
        return relationships;
    }

    private Connector getConnector(final ProcessContext context) throws AccumuloException, AccumuloSecurityException {
        getLogger().info("Getting Accumulo connector.");
        final String instanceName = context.getProperty(INSTANCE_NAME).getValue();
        final String zookeeperConnString = context.getProperty(ZOOKEEPER_CONNECT_STRING).getValue();
        final Instance instance = new ZooKeeperInstance(instanceName, zookeeperConnString);
        final String username = context.getProperty(USERNAME).getValue();
        final String password = context.getProperty(PASSWORD).getValue();
        return instance.getConnector(username, new PasswordToken(password));
    }

    public AccumuloEventStore getEventStore(final ProcessContext context) throws AccumuloSecurityException, AccumuloException, TableExistsException, TableNotFoundException {
        getLogger().info("Getting Accumulo event store.");
        Connector conn = getConnector(context);
        return new AccumuloEventStore(conn);
    }

    public GraphStore getGraphStore(final ProcessContext context) throws AccumuloException, AccumuloSecurityException, TableNotFoundException, TableExistsException {
        getLogger().info("Getting Accumulo graph store");
        Connector conn = getConnector(context);
        return new AccumuloEntityGraphStore(conn);
    }

}
